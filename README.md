# css-tailwind-preview

This is a dummy project to help showing what Tailwind is, what it aims to help us with and what are the drawbacks. The project is really simple, we have couple of example components styled only with Tailwind under `src/components/`, which are loaded in the main app file. If you start the project with `yarn dev` you will see all of them imported in the main file. The file `RealNavigationExample.vue` is a copy from another project as a "real" example how TW can look at the end (not sayin that this implementation is perfect, note this **really hard**).

## Recording
The recording of this meeting can be found over here: [https://blubitoag-my.sharepoint.com/:v:/g/personal/p_ivanova_blubito_com/EcAEz6WBoUZFng53fbNPygABcVwbtbvso9Rib_nEXXXyqg](https://blubitoag-my.sharepoint.com/:v:/g/personal/p_ivanova_blubito_com/EcAEz6WBoUZFng53fbNPygABcVwbtbvso9Rib_nEXXXyqg)


## Project setup
```
yarn install
```

## Compiles and hot-reloads for development
```
yarn dev
```
